import React, {Component, PropTypes} from 'react';
import {Text, View, NativeModules, StyleSheet} from 'react-native';
import Button from '../../components/Kit/Button';
import Input from '../../components/Kit/Input';
import {grayDark} from './../../variables';
import PersianText from '../../components/Kit/PersianText';

class FileInput extends Component {
  static propTypes = {
    containerStyle: View.propTypes.style,
    style: Text.propTypes.style,
    type: PropTypes.oneOf(['primary', 'success', 'danger', 'warning', 'default']),
    onPress: PropTypes.func,
    onFilePicked: PropTypes.func,
    onFailure: PropTypes.func,
    outline: PropTypes.bool,
    loading: PropTypes.bool,
    question: PropTypes.object,
    fields: PropTypes.object
  };

  state = {
    uri: ""
  };

  onChange;

  componentDidMount() {
    const {onChange} = this.props.field;
    this.onChange = onChange;
  }

  pickAFile() {
    return new Promise((resolve, reject) => {
      NativeModules.FilePicker.pickFile().then(
        uri => resolve(uri),
        error => reject(error)
      );
    });
  }

  highOrderOnPress = () => {
    const {onPress, onFailure, onFilePicked} = this.props;
    this.pickAFile().then(
      uri => {
        onFilePicked(uri);
        this.setState({uri});
        this.onChange(uri);
      },
      error => onFailure(error)
    );

    onPress();
  };

  render() {
    const {children, field, question, ...rest} = this.props;
    const {title} = question;

    return (
      <View>
        <PersianText rtl type="light" style={styles.label}>{title}</PersianText>

        <Button {...rest} onPress={this.highOrderOnPress}>
          {children}
        </Button>

        {this.state.uri.length > 0 ?
          <PersianText type="light" style={styles.label}>{this.state.uri}</PersianText> : null}
        <View style={styles.hiddenInput}>
          <Input {...field} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  label: {
    color: grayDark
  },
  hiddenInput: {
    width: 0,
    height: 0
  }
});

export default FileInput;
