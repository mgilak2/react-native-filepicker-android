package me.gilak.filepicker;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.nononsenseapps.filepicker.FilePickerActivity;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;


public class FilePickerModule extends ReactContextBaseJavaModule implements ActivityEventListener {

    private Promise mPickerPromise;
    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
    private static final String E_NO_IMAGE_DATA_FOUND = "E_NO_IMAGE_DATA_FOUND";

    public FilePickerModule(ReactApplicationContext reactContext) {
        super(reactContext);

        // Add the listener for `onActivityResult`
        reactContext.addActivityEventListener(this);
    }

    @Override
    public String getName() {
        return "FilePicker";
    }

    @ReactMethod
    public void pickFile(final Promise promise) {
        Activity currentActivity = getCurrentActivity();

        if (currentActivity == null) {
            promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
            return;
        }

        mPickerPromise = promise;

        Intent i = new Intent(currentActivity, FilePickerActivity.class);

        i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);

        i.putExtra(FilePickerActivity.EXTRA_START_PATH, Environment.getExternalStorageDirectory().getPath());

        currentActivity.startActivityForResult(i, 1);
    }

    // You can get the result here
    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
                // For JellyBean and above
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = data.getClipData();

                    if (clip != null) {
                        for (int i = 0; i < clip.getItemCount(); i++) {
                            Uri uri = clip.getItemAt(i).getUri();
                            if (uri == null) {
                                final File myFile;
                                try {
                                    myFile = new File(new URI(uri.toString()));
                                    final String absolute = myFile.getAbsolutePath();
                                    mPickerPromise.reject(E_NO_IMAGE_DATA_FOUND, absolute);
                                } catch (URISyntaxException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                mPickerPromise.resolve(uri.toString());
                            }
                        }
                    }
                    // For Ice Cream Sandwich
                } else {
                    ArrayList<String> paths = data.getStringArrayListExtra
                            (FilePickerActivity.EXTRA_PATHS);

                    if (paths != null) {
                        for (String path : paths) {
                            Uri uri = Uri.parse(path);
                            if (uri == null) {
                                mPickerPromise.reject(E_NO_IMAGE_DATA_FOUND, "No image data found");
                            } else {
                                mPickerPromise.resolve(uri.toString());
                            }
                        }
                    }
                }

            } else {
                Uri uri = data.getData();
                if (uri == null) {
                    mPickerPromise.reject(E_NO_IMAGE_DATA_FOUND, "No image data found");
                } else {
                    mPickerPromise.resolve(uri.toString());
                }
            }
        }

    }

}
