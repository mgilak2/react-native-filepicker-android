#React Native Filepicker Android

you know [NoNonsense-FilePicker](https://github.com/spacecowboy/NoNonsense-FilePicker/) on Android ? now you have it in react native at your command :)


note: you should apply every config of NoNonsense-FilePicker on your project manually. also 
I did provide an example :()


###Usage

```js
import React, {Component, PropTypes} from 'react';
import {Text, View, NativeModules, StyleSheet, Text} from 'react-native';
import Button from './UI/Button';
import Input from './UI/Input';
import {grayDark} from './colors';

class FileInput extends Component {
  static propTypes = {
    containerStyle: View.propTypes.style,
    style: Text.propTypes.style,
    type: PropTypes.oneOf(['primary', 'success', 'danger', 'warning', 'default']),
    onPress: PropTypes.func,
    onFilePicked: PropTypes.func,
    onFailure: PropTypes.func,
    outline: PropTypes.bool,
    loading: PropTypes.bool,
    question: PropTypes.object,
    fields: PropTypes.object
  };

  state = {
    uri: ""
  };

  onChange;

  componentDidMount() {
    const {onChange} = this.props.field;
    this.onChange = onChange;
  }

  pickAFile() {
    return new Promise((resolve, reject) => {
      NativeModules.FilePicker.pickFile().then(
        uri => resolve(uri),
        error => reject(error)
      );
    });
  }

  highOrderOnPress = () => {
    const {onPress, onFailure, onFilePicked} = this.props;
    this.pickAFile().then(
      uri => {
        onFilePicked(uri);
        this.setState({uri});
        this.onChange(uri);
      },
      error => onFailure(error)
    );

    onPress();
  };

  render() {
    const {children, field, question, ...rest} = this.props;
    const {title} = question;

    return (
      <View>
        <Text type="light" style={styles.label}>{title}</Text>

        <Button {...rest} onPress={this.highOrderOnPress}>
          {children}
        </Button>

        {this.state.uri.length > 0 ?
          <Text type="light" style={styles.label}>{this.state.uri}</Text> : null}
        <View style={styles.hiddenInput}>
          <Input {...field} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  label: {
    color: grayDark
  },
  hiddenInput: {
    width: 0,
    height: 0
  }
});

export default FileInput;
     
```

###MainActivity.java

```java
package me.gilak.sample;

import android.content.Intent;


import me.gilak.filepicker.FilePickerPackage;

import com.facebook.react.ReactActivity;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends ReactActivity {
    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    @Override
    protected String getJSBundleFile() {
        return CodePush.getBundleUrl();
    }

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "EvandDiscovery";
    }

    /**
     * Returns whether dev mode should be enabled.
     * This enables e.g. the dev menu.
     */
    @Override
    protected boolean getUseDeveloperSupport() {
        return BuildConfig.DEBUG;
    }

    /**
     * A list of packages used by the app. If the app uses additional views
     * or modules besides the default ones, add more packages here.
     */
    @Override
    protected List<ReactPackage> getPackages() {
        return Arrays.<ReactPackage>asList(
                new FilePickerPackage(),
        );
    }
}


```

#an example of res/values/style.xml

```xml
<resources>

    <!-- Base application theme. -->
    <style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar">
        <!-- Customize your theme here. -->
    </style>

    <style name="FilePickerTheme" parent="NNF_BaseTheme">
        <!-- Set these to match your theme -->
        <item name="colorPrimary">#db1c1c</item>
        <item name="colorPrimaryDark">#ef7f00</item>
        <item name="colorAccent">#16e0dd</item>

        <!-- Setting a divider is entirely optional -->

        <!-- Need to set this also to style create folder dialog -->
        <item name="alertDialogTheme">@style/FilePickerAlertDialogTheme</item>

        <!-- If you want to set a specific toolbar theme, do it here -->
        <!-- <item name="nnf_toolbarTheme">@style/ThemeOverlay.AppCompat.Dark.ActionBar</item> -->
    </style>

    <style name="FilePickerAlertDialogTheme" parent="Theme.AppCompat.Dialog.Alert">
        <item name="colorPrimary">#1ace5f</item>
        <item name="colorPrimaryDark">#d3bc0c</item>
        <item name="colorAccent">#d60a00</item>
    </style>

</resources>

```

### AndroidManifest.xml
```
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="me.gilak.sample"
    android:versionCode="1"
    android:versionName="1.0">

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW"/>
    <uses-permission android:name="android.permission.READ_CONTACTS" />
    <uses-permission android:name="android.permission.WRITE_CONTACTS" />

    <uses-sdk
        android:minSdkVersion="16"
        android:targetSdkVersion="22" />

    <application
      android:name=".MainApplication"
      android:allowBackup="true"
      android:label="@string/app_name"
      android:icon="@mipmap/ic_launcher"
      android:theme="@style/AppTheme">
      <activity
        android:name=".MainActivity"
        android:label="@string/app_name"
        android:configChanges="keyboard|keyboardHidden|orientation|screenSize">
        <intent-filter>
            <action android:name="android.intent.action.MAIN" />
            <category android:name="android.intent.category.LAUNCHER" />
        </intent-filter>
      </activity>
      <activity android:name="com.facebook.react.devsupport.DevSettingsActivity" />
          <activity
        android:name="com.nononsenseapps.filepicker.FilePickerActivity"
        android:label="@string/app_name"
        android:theme="@style/FilePickerTheme">
      <intent-filter>
        <action android:name="android.intent.action.GET_CONTENT" />
        <category android:name="android.intent.category.DEFAULT" />
      </intent-filter>
    </activity>

    </application>

</manifest>

```

#####Any contributions are welcome
